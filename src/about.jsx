import React from 'react';

function AboutHTML() {
    const body = {border: "2px solid gainsboro"};
    const header = {textAlign: "center"};
    const li = {listStylePosition: "inside"}
    return(
        <div>
            <meta charSet="UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>b.membuat file about</title>
            <header style={{ textAlign: "center" }}>
              <h1>Data Peserta Sanbercode Bootcamp Reactjs</h1>
            </header>
            <content>
              <ol>
                <li style={{ listStylePosition: "inside" }}>
                  <strong>Nama:</strong> nama peserta
                </li>
                <li style={{ listStylePosition: "inside" }}>
                  <strong>Email:</strong> email peserta
                </li>
                <li style={{ listStylePosition: "inside" }}>
                  <strong>Sistem Operasi yang digunakan:</strong> sistem operasi peserta
                </li>
                <li style={{ listStylePosition: "inside" }}>
                  <strong>Akun Gitlab:</strong> akun gitlab peserta
                </li>
                <li style={{ listStylePosition: "inside" }}>
                  <strong>Akun Telegram:</strong> akun telegram peserta
                </li>
              </ol>
              <a href="index.html">Kembali Ke Index</a>
            </content>
          </div>
    )
}

export default AboutHTML;