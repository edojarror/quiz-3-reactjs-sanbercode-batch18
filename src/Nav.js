import React, { useContext } from "react"
import { Link } from "react-router-dom";

import logo from './logo.png';
import './App.css';
import './style.css';

const Nav = () =>{
    
  return(
      <div>
          <nav>
        <ul>
        <img id="logo" src={logo} width="200px" />
        <li className="navbar-li">
            <Link to="/Login">
                Login
            </Link>
        </li>
        <li className="navbar-li">
            <Link to="/Movie">
                Movie Editor List
            </Link>
        </li>
        
        <li className="navbar-li">
            <Link to="/about">
                About
            </Link>
        </li>
        
        <li className="navbar-li">
            <Link to="/">
                Home
            </Link>
        </li>
        
        
        </ul>
    </nav>
      </div>
    
  )
}

export default Nav;
