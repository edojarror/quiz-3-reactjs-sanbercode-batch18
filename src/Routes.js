import React from 'react';
import axios from 'axios';
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import Nav from './Nav';
import About from './about';
import Home from './Home';
import Login from './Login';
import Movie from './Movie';
import logo from './logo.png';
import './App.css';
import './style.css';

export default function App() {
    return (
        <div>
            <Router>
                <Nav />
                <Switch>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/movie">
                        <Movie />
                    </Route>
                    <Route path="/about">
                        <About />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </Router>

        </div>
  );
}
