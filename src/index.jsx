import React from 'react';
import logo from './logo.svg';
import './style.css';

class Indexjs extends React.Component {
    constructor() {
        super()
    }
    render() {
        return(
            <div>
      <link href="./style.css" rel="stylesheet" type="text/css" />
      <link
        href="https://fonts.googleapis.com/css?family=Slabo+27px"
        rel="stylesheet"
      />
      <header>
        <nav>
          <ul>
            <img id="logo" src={logo} width="200px" />
            <a className="navbar-li" href="index.html">
              <li>Home</li>
            </a>
            <a className="navbar-li" href="about.html">
              <li>About</li>
            </a>
            <a className="navbar-li" href="contact.html">
              <li>Contact</li>
            </a>
          </ul>
        </nav>
      </header>
      <section>
        <h1>Featured Posts</h1>
        <div id="article-list">
          <div>
            <a href>
              <h3>Lorem Post 1</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod
              deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut.
              Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href>
              <h3>Lorem Post 2</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod
              deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut.
              Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href>
              <h3>Lorem Post 3</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod
              deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut.
              Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href>
              <h3>Lorem Post 4</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod
              deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut.
              Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
          </div>
          <div>
            <a href>
              <h3>Lorem Post 5</h3>
            </a>
            <p>
              Lorem Ipsum Dolor Sit Amet, mea te verear signiferumque, per illum
              labores ne. Blandit omnesque scripserit pri ex, et pri dicant eirmod
              deserunt. Aeque perpetua ea nec. Sit erant patrioque delicatissimi ut.
              Et sea quem sint, nam in minim voluptatibus. Etiam placerat eam in.
            </p>
          </div>
        </div>
      </section>
      <footer>
        <h5>copyright © 2020 by Sanbercode</h5>
      </footer>
    </div>
        )
    }
    
    }
    

export default Indexjs;